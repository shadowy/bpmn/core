package old

type LinkData struct {
	PParent *LinkParentData `json:"parent" yaml:"parent"`
	PChild  string          `json:"child" yaml:"child"`
}

func LinkToLinkData(link Link) *LinkData {
	res := new(LinkData)
	res.PChild = link.Child()
	res.PParent = new(LinkParentData)
	res.PParent.PID = link.Parent().ID()
	res.PParent.POutput = link.Parent().Output()
	return res
}

func (d *LinkData) Parent() LinkParent {
	return d.PParent
}

func (d *LinkData) Child() string {
	return d.PChild
}
