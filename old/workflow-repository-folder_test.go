package old

import (
	"gitlab.com/shadowy/go/dependency-injection"
	"testing"
)

func FactoryActionRepository() (interface{}, error) {
	return NewActionRepositoryFolder("./data/action")
}

func FactoryWorkflowRepository() (interface{}, error) {
	return DINewWorkflowRepositoryFolder("./data/workflow")
}

func init() {
	di.Add("WorkflowRepository", true, FactoryWorkflowRepository)
}

func TestCreateWorkflow(t *testing.T) {
	res, err := di.Create[WorkflowRepository]("WorkflowRepository")
	if err != nil {
		t.Error(err)
		return
	}
	if res == nil {
		t.Error("res should be not null")
		return
	}
	versions := res.GetVersions("example-001")
	if len(versions) != 1 {
		t.Error("len(versions) should be 1")
		return
	}
	workflow := res.Get("example-001", versions[0].Version)
	if workflow == nil {
		t.Error("workflow should be not nil")
		return
	}
}
