package old

import (
	"encoding/json"
	"gopkg.in/yaml.v3"
)

type WorkflowData struct {
	PID          string          `json:"id" yaml:"id"`
	PName        string          `json:"name" yaml:"name"`
	PDescription string          `json:"description" yaml:"description"`
	PActivities  []*ActivityData `json:"activities" yaml:"activities"`
	PLinks       []*LinkData     `json:"links" yaml:"links"`
}

func LoadWorkflowDataFromYaml(data string) (*WorkflowData, error) {
	res := new(WorkflowData)
	if err := yaml.Unmarshal([]byte(data), res); err != nil {
		return nil, err
	}
	return res, nil
}

func (d *WorkflowData) ID() string {
	return d.PID
}

func (d *WorkflowData) Name() string {
	return d.PName
}

func (d *WorkflowData) Description() string {
	return d.PDescription
}

func (d *WorkflowData) Activities() []Activity {
	var res []Activity
	for i := range d.PActivities {
		res = append(res, d.PActivities[i])
	}
	return res
}

func (d *WorkflowData) Links() []Link {
	var res []Link
	for i := range d.PLinks {
		res = append(res, d.PLinks[i])
	}
	return res
}

func (d *WorkflowData) Validate() (result bool, errors []error) {
	return false, nil
}

func (d *WorkflowData) NextActivities(activity Activity) []Activity {
	return nil
}

func (d *WorkflowData) ToYAML() string {
	res, _ := yaml.Marshal(d)
	return string(res)
}

func (d *WorkflowData) ToJSON() string {
	res, _ := json.Marshal(d)
	return string(res)
}
