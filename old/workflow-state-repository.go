package old

type WorkflowStateRepository interface {
	Create(workflowID string, workflowVersion uint, properties map[string]string) (WorkflowState, error)
	Get(id string) (WorkflowState, error)
	Save(state WorkflowState) error
}
