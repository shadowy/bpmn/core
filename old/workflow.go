package old

type Workflow interface {
	ID() string
	Name() string
	Description() string
	Activities() []Activity
	Links() []Link

	// Validate - validate workflow
	Validate() (result bool, errors []error)
	// NextActivities  - return next activities which should be executed after this
	NextActivities(activity Activity) []Activity

	// ToYAML - return yaml presentation of workflow
	ToYAML() string
	// ToJSON - return json presentation of workflow
	ToJSON() string
}
