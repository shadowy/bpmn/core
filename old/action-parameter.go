package old

type ActionParameter interface {
	ID() string
	Type() string
	DefaultValue() string
	Name() string
	Description() string
	Required() bool
}
