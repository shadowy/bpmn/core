package old

type ActionParameterData struct {
	PID           string `yaml:"id"`
	PType         string `yaml:"type"`
	PDefaultValue string `yaml:"defaultValue"`
	PName         string `yaml:"name"`
	PDescription  string `yaml:"description"`
	PRequired     bool   `yaml:"required"`
}

func ActionParameterToActionParameterData(parameter ActionParameter) *ActionParameterData {
	res := new(ActionParameterData)
	res.PID = parameter.ID()
	res.PType = parameter.Type()
	res.PDefaultValue = parameter.DefaultValue()
	res.PName = parameter.Name()
	res.PDescription = parameter.Description()
	res.PRequired = parameter.Required()
	return res
}

func (p *ActionParameterData) ID() string {
	return p.PID
}

func (p *ActionParameterData) Type() string {
	return p.PType
}

func (p *ActionParameterData) DefaultValue() string {
	return p.PDefaultValue
}

func (p *ActionParameterData) Name() string {
	return p.PName
}

func (p *ActionParameterData) Description() string {
	return p.PDescription
}

func (p *ActionParameterData) Required() bool {
	return p.PRequired
}
