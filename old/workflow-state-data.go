package old

import (
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"gopkg.in/yaml.v3"
)

type WorkflowVersion struct {
	ID      string `json:"id" yaml:"id"`
	Version uint   `json:"version" yaml:"version"`
}

type WorkflowStateData struct {
	PID         string                `json:"id" yaml:"id"`
	PWorkflow   WorkflowVersion       `json:"workflow" yaml:"workflow"`
	PProperties map[string]string     `json:"properties" yaml:"properties"`
	PChanges    []WorkflowStateChange `json:"changes" yaml:"changes"`
	PActivities []*ActivityStateData  `json:"activities" yaml:"activities"`
	PLinks      []*LinkData           `json:"links" yaml:"links"`

	workflow Workflow
}

func LoadWorkflowStateDataFromYaml(data string, repository WorkflowRepository) (*WorkflowStateData, error) {
	res := new(WorkflowStateData)
	if err := yaml.Unmarshal([]byte(data), res); err != nil {
		return nil, err
	}
	if err := res.init(repository); err != nil {
		return nil, err
	}
	return res, nil
}

func NewWorkflowStateDataFromYAML(data string, repository WorkflowRepository) (*WorkflowStateData, error) {
	res := new(WorkflowStateData)
	if err := yaml.Unmarshal([]byte(data), res); err != nil {
		return nil, err
	}
	if err := res.init(repository); err != nil {
		return nil, err
	}
	return res, nil
}

func NewWorkflowStateData(workflowID string, workflowVersion uint, repository WorkflowRepository) (*WorkflowStateData, error) {
	res := new(WorkflowStateData)
	res.PWorkflow.ID = workflowID
	res.PWorkflow.Version = workflowVersion
	if err := res.init(repository); err != nil {
		return nil, err
	}
	return res, nil
}

func (d *WorkflowStateData) init(repository WorkflowRepository) error {
	d.workflow = repository.Get(d.PWorkflow.ID, d.PWorkflow.Version)
	if d.workflow == nil {
		return errors.New("workflow not found")
	}
	if d.PID == "" {
		d.PID = uuid.New().String()
	}
	if d.PProperties == nil {
		d.PProperties = make(map[string]string)
	}
	if d.PActivities == nil {
		list := d.workflow.Activities()
		for i := range list {
			d.PActivities = append(d.PActivities, NewActivityStateData(list[i]))
		}
	}
	if d.PLinks == nil {
		list := d.workflow.Links()
		for i := range list {
			d.PLinks = append(d.PLinks, LinkToLinkData(list[i]))
		}
	}
	return nil
}

func (d *WorkflowStateData) ID() string {
	return d.PID
}

func (d *WorkflowStateData) Workflow() Workflow {
	return d.workflow
}

func (d *WorkflowStateData) Activities() []ActivityState {
	var res []ActivityState
	for i := range d.PActivities {
		res = append(res, d.PActivities[i])
	}
	return res
}

func (d *WorkflowStateData) Links() []Link {
	var res []Link
	for i := range d.PLinks {
		res = append(res, d.PLinks[i])
	}
	return res
}

func (d *WorkflowStateData) Properties() map[string]string {
	return d.PProperties
}

func (d *WorkflowStateData) SetProperty(name, value string) {
	d.PProperties[name] = value
}

func (d *WorkflowStateData) GetProperty(name string) string {
	return d.PProperties[name]
}

func (d *WorkflowStateData) Changes() []WorkflowStateChange {
	return d.PChanges
}

func (d *WorkflowStateData) Validate() (result bool, errors []error) {
	return false, nil
}

func (d *WorkflowStateData) ToYAML() string {
	res, _ := yaml.Marshal(d)
	return string(res)
}

func (d *WorkflowStateData) ToJSON() string {
	res, _ := json.Marshal(d)
	return string(res)
}
