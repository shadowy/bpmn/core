package old

// Action - prototype for activity
type Action interface {
	ID() string
	Name() string
	Description() string
	Outputs() []string
	Parameters() []ActionParameter
}
