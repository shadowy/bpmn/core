package old

type WorkflowStateChange struct {
	ActionID   string
	State      string
	Properties map[string]string
	// Properties TBD
}

type WorkflowState interface {
	ID() string
	Workflow() Workflow
	Activities() []ActivityState
	Links() []Link

	Properties() map[string]string
	SetProperty(name, value string)
	GetProperty(name string) string

	// Changes - return all changes step by step
	// return type will be TBD
	Changes() []WorkflowStateChange

	// Validate - validate workflow state
	Validate() (result bool, errors []error)

	// ToYAML - return yaml presentation of workflow state
	ToYAML() string
	// ToJSON - return json presentation of workflow state
	ToJSON() string
}
