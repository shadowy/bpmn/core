module gitlab.com/shadowy/bmpn/core

go 1.19

require (
	github.com/google/uuid v1.3.0 // indirect
	gitlab.com/shadowy/go/dependency-injection v0.1.2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
