package old

import (
	"os"
	"path"
	"strings"
)

type ActionRepositoryFolder struct {
	folder       string
	actionsIndex map[string]*ActionData
	list         []*ActionData
	outList      []Action
}

func NewActionRepositoryFolder(folder string) (ActionRepository, error) {
	res := new(ActionRepositoryFolder)
	res.folder = folder
	if err := res.init(); err != nil {
		return nil, err
	}
	return res, nil
}

func (r *ActionRepositoryFolder) init() error {
	r.actionsIndex = make(map[string]*ActionData)
	files, err := os.ReadDir(r.folder)
	if err != nil {
		return err
	}
	for i := range files {
		file := files[i].Name()

		if strings.ToLower(path.Ext(file)) != ".yaml" {
			continue
		}
		data, err := os.ReadFile(path.Join(r.folder, file))
		if err != nil {
			return err
		}
		var action *ActionData
		action, err = LoadActionDataFromYaml(string(data))
		if err != nil {
			return err
		}
		r.actionsIndex[action.PID] = action
		r.list = append(r.list, action)
		r.outList = append(r.outList, action)
	}
	return nil
}

func (r *ActionRepositoryFolder) Get(id string) Action {
	if res, ok := r.actionsIndex[id]; ok {
		return res
	}
	return nil
}

func (r *ActionRepositoryFolder) List() []Action {
	return r.outList
}
