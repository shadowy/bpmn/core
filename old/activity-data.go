package old

type ActivityData struct {
	PID          string     `yaml:"id"`
	PName        string     `yaml:"name"`
	PDescription string     `yaml:"description"`
	PAction      ActionData `yaml:"action"`
}

func ActivityToActivityData(activity Activity) *ActivityData {
	res := new(ActivityData)
	res.PID = activity.ID()
	res.PName = activity.Name()
	res.PDescription = activity.Description()
	res.PAction = *ActionToActionData(activity.Action())
	return res
}

func (p *ActivityData) ID() string {
	return p.PID
}

func (p *ActivityData) Name() string {
	return p.PName
}

func (p *ActivityData) Description() string {
	return p.PDescription
}

func (p *ActivityData) Action() Action {
	return &p.PAction
}
