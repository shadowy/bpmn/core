package old

import "testing"

func TestInit(t *testing.T) {
	repository, err := NewActionRepositoryFolder("./data/action")
	if err != nil {
		t.Error(err)
	}
	if len(repository.List()) != 4 {
		t.Error("number of action should be 4")
	}
	action := repository.Get("unknown")
	if action != nil {
		t.Error("action should be null")
	}
	action = repository.Get("start")
	if action == nil {
		t.Error("action should be not null")
	}
	if len(action.Outputs()) != 1 {
		t.Error("action.output should be 1")
	}
}
