package old

import (
	di "gitlab.com/shadowy/go/dependency-injection"
	"testing"
)

func FactoryWorkflowStateRepository() (interface{}, error) {
	return DINewWorkflowStateRepositoryFolder("./data/state")
}

func init() {
	di.Add("WorkflowRepository", true, FactoryWorkflowRepository)
	di.Add("WorkflowStateRepository", true, FactoryWorkflowStateRepository)
}

func TestCreateWorkflowState(t *testing.T) {
	res, err := di.Create[WorkflowStateRepository]("WorkflowStateRepository")
	if err != nil {
		t.Error(err)
		return
	}
	if res == nil {
		t.Error("res should be not null")
		return
	}

	var workflowState WorkflowState
	workflowState, err = res.Create("example-001", 1, map[string]string{"param1": "value1"})
	if err != nil {
		t.Error(err)
		return
	}
	if workflowState == nil {
		t.Error("workflowState should be not null")
		return
	}
	err = res.Save(workflowState)
	if err != nil {
		t.Error(err)
		return
	}

	ws, err := res.Get(workflowState.ID())
	if err != nil {
		t.Error(err)
		return
	}
	if ws == nil {
		t.Error("ws should be not null")
		return
	}
	if ws.ToYAML() != workflowState.ToYAML() {
		t.Error("workflow states should be equal")
		return
	}
}
