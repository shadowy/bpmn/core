package old

import (
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
)

type WorkflowRepositoryFolder struct {
	folder string
}

func NewWorkflowRepositoryFolder(folder string, actionRepository ActionRepository) WorkflowRepository {
	res := new(WorkflowRepositoryFolder)
	res.folder = folder
	return res
}

func DINewWorkflowRepositoryFolder(folder string) (WorkflowRepository, error) {
	res := new(WorkflowRepositoryFolder)
	res.folder = folder
	return res, nil
}

func (r *WorkflowRepositoryFolder) Get(id string, version uint) Workflow {
	filename := fmt.Sprintf("%s~%d.yaml", id, version)
	data, err := os.ReadFile(path.Join(r.folder, filename))
	if err != nil {
		return nil
	}
	var workflow *WorkflowData
	workflow, err = LoadWorkflowDataFromYaml(string(data))
	if err != nil {
		return nil
	}
	return workflow
}

func (r *WorkflowRepositoryFolder) GetVersions(id string) []*WorkflowPossibleVersion {
	files, err := os.ReadDir(r.folder)
	if err != nil {
		return nil
	}
	var res []*WorkflowPossibleVersion
	for i := range files {
		file := files[i]
		if strings.ToLower(path.Ext(file.Name())) != ".yaml" {
			continue
		}

		parts := strings.Split(path.Base(file.Name()), "~")
		if parts[0] != id {
			continue
		}
		ver, err := strconv.ParseUint(
			strings.Split(parts[1], ".")[0],
			10,
			32)
		if err != nil {
			continue
		}
		res = append(res, &WorkflowPossibleVersion{
			Version: uint(ver),
			Default: false,
		})
	}
	return res
}

func (r *WorkflowRepositoryFolder) SetCurrent(id string, version uint) error {
	return nil
}

func (r *WorkflowRepositoryFolder) Save(workflow Workflow) Workflow {
	return nil
}
