package old

type Link interface {
	Parent() LinkParent
	Child() string
}
