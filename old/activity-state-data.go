package old

type ActivityStateData struct {
	ActivityData `json:"activity" yaml:"activity"`

	PState string `json:"state" yaml:"state"`
}

func NewActivityStateData(activity Activity) *ActivityStateData {
	res := new(ActivityStateData)
	res.ActivityData = *ActivityToActivityData(activity)
	res.PState = "not-executed"
	return res
}
func (d *ActivityStateData) State() string {
	return d.PState
}
