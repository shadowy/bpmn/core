package old

import (
	"fmt"
	"os"
	"path"
)

type WorkflowStateRepositoryFolder struct {
	folder string

	WorkflowRepository WorkflowRepository `inject:"WorkflowRepository"`
}

func NewWorkflowStateRepositoryFolder(folder string, workflowRepository WorkflowRepository) WorkflowStateRepository {
	res := new(WorkflowStateRepositoryFolder)
	res.folder = folder
	res.WorkflowRepository = workflowRepository
	return res
}

func DINewWorkflowStateRepositoryFolder(folder string) (WorkflowStateRepository, error) {
	res := new(WorkflowStateRepositoryFolder)
	res.folder = folder
	return res, nil
}

func (r *WorkflowStateRepositoryFolder) Create(workflowID string, workflowVersion uint, properties map[string]string) (WorkflowState, error) {
	if res, err := NewWorkflowStateData(workflowID, workflowVersion, r.WorkflowRepository); err != nil {
		return nil, err
	} else {
		res.PProperties = properties
		return res, nil
	}
}

func (r *WorkflowStateRepositoryFolder) Get(id string) (WorkflowState, error) {
	filename := path.Join(r.folder, fmt.Sprintf("%s.yaml", id))
	if data, err := os.ReadFile(filename); err != nil {
		return nil, err
	} else {
		var res WorkflowState
		if res, err = NewWorkflowStateDataFromYAML(string(data), r.WorkflowRepository); err != nil {
			return nil, err
		}
		return res, nil
	}
}

func (r *WorkflowStateRepositoryFolder) Save(state WorkflowState) error {
	filename := path.Join(r.folder, fmt.Sprintf("%s.yaml", state.ID()))
	data := []byte(state.ToYAML())
	if err := os.WriteFile(filename, data, 0644); err != nil {
		return err
	}
	return nil
}
