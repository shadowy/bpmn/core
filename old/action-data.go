package old

import "gopkg.in/yaml.v3"

type ActionData struct {
	PID          string                 `yaml:"id"`
	PName        string                 `yaml:"name"`
	PDescription string                 `yaml:"description"`
	POutputs     []string               `yaml:"outputs"`
	PParameters  []*ActionParameterData `yaml:"parameters"`
}

func ActionToActionData(action Action) *ActionData {
	res := new(ActionData)
	res.PID = action.ID()
	res.PName = action.Name()
	res.PDescription = action.Description()
	res.POutputs = action.Outputs()
	list := action.Parameters()
	for i := range list {
		res.PParameters = append(res.PParameters, ActionParameterToActionParameterData(list[i]))
	}
	return res
}

func LoadActionDataFromYaml(data string) (*ActionData, error) {
	res := new(ActionData)
	if err := yaml.Unmarshal([]byte(data), res); err != nil {
		return nil, err
	}
	return res, nil
}

func (p *ActionData) ID() string {
	return p.PID
}

func (p *ActionData) Name() string {
	return p.PName
}

func (p *ActionData) Description() string {
	return p.PDescription
}

func (p *ActionData) Outputs() []string {
	return p.POutputs
}

func (p *ActionData) Parameters() []ActionParameter {
	var res []ActionParameter
	for i := range p.PParameters {
		res = append(res, p.PParameters[i])
	}
	return res
}
