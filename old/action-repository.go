// Package core contains base interfaces for working with BMPN
package old

// ActionRepository - Repository for work with Actions
type ActionRepository interface {
	// Get action by ID
	Get(id string) Action
	// List get all available actions
	List() []Action
}
