package old

type Activity interface {
	ID() string
	Name() string
	Description() string
	Action() Action
}
