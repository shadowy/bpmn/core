package old

type LinkParentData struct {
	PID     string `json:"id" yaml:"id"`
	POutput string `json:"output" yaml:"output"`
}

func (d *LinkParentData) ID() string {
	return d.PID
}

func (d *LinkParentData) Output() string {
	return d.POutput
}
