package old

type WorkflowPossibleVersion struct {
	Version uint
	Default bool
}

type WorkflowRepository interface {
	// Get - return workflow by id and version (if version = 0 returned current version of workflow)
	Get(id string, version uint) Workflow
	// GetVersions - return list of all possible versions for workflow with ID
	GetVersions(id string) []*WorkflowPossibleVersion
	// SetCurrent - set current version for workflow
	SetCurrent(id string, version uint) error
	// Save - storing workflow and set new version for it (without changing default version of workflow
	Save(workflow Workflow) Workflow
}
